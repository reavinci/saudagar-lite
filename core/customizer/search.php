<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Search extends Customizer_Base
{

    public function __construct()
    {

        $this->set_section();

        $this->add_search_option();
    }

    public function set_section()
    {
        $this->add_section('', array(
            'search_option' => array(esc_attr__('Search ', 'rt_domain')),
        ));
    }

    public function add_search_option()
    {
        $section = 'search_option_section';
        $settings = 'search_options';

        $this->add_field(array(
            'type' => 'select',
            'section' => $section,
            'settings' => $settings . '_result',
            'label' => __('Post Types Result', 'rt_domain'),
            'default' => 'post',
            'multiple' => 1,
            'choices' => apply_filters('rt_post_type_search_result', array(
                'post' => 'Post',
                'product' => 'Product',
            )),
        ));

        $this->add_field(array(
            'type' => 'text',
            'section' => $section,

            'settings' => $settings . '_text',
            'label' => __('Placeholder', 'rt_domain'),
            'default' => 'Type Something and enter',
        ));

       
    }

// end class
}

new Search;
