<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Sidebar extends Customizer_Base
{
    public function __construct()
    {
        $this->set_section();

        $this->add_option();
        $this->add_layout();
    }

    public function set_section()
    {
        $this->add_section('', array(
            'sidebar_option' => array(esc_attr__('Sidebar', 'rt_domain')),
            'sidebar_layout' => array(esc_attr__('Sidebar - Layout', 'rt_domain')),
            'sidebar_Element' => array(esc_attr__('Sidebar - Element', 'rt_domain')),
        ));
    }

    public function add_option()
    {
       
    }
    public function add_layout()
    {
        $section = 'sidebar_option_section';
        $settings = 'sidebar_layout';

        $this->add_field(array(
            'type' => 'radio-buttonset',
            'settings' => $settings . '_home',
            'label' => __('Sidebar On Home Page', 'rt_domain'),
            'section' => $section,
            'default' => 'right',
            'choices' => array(
                'none' => __('No Sidebar', 'rt_domain'),
                'left' => __('Left', 'rt_domain'),
                'right' => __('Right', 'rt_domain'),
            ),
        ));

        $this->add_field(array(
            'type' => 'radio-buttonset',
            'settings' => $settings . '_archive',
            'label' => __('Sidebar On Archive Page', 'rt_domain'),
            'section' => $section,
            'default' => 'right',
            'choices' => array(
                'none' => __('No Sidebar', 'rt_domain'),
                'left' => __('Left', 'rt_domain'),
                'right' => __('Right', 'rt_domain'),
            ),
        ));

        if(rt_var('gutenberg-support')){
            $this->add_field(array(
                'type' => 'radio-buttonset',
                'settings' => $settings . '_single',
                'label' => __('Sidebar On Single Page', 'rt_domain'),
                'section' => $section,
                'default' => 'right',
                'choices' => array(
                    'none' => __('No Sidebar', 'rt_domain'),
                    'left' => __('Left', 'rt_domain'),
                    'right' => __('Right', 'rt_domain'),
                ),
            ));

            $this->add_field(array(
                'type' => 'radio-buttonset',
                'settings' => $settings . '_page',
                'label' => __('Sidebar On Regular Page', 'rt_domain'),
                'section' => $section,
                'default' => 'right',
                'choices' => array(
                    'none' => __('No Sidebar', 'rt_domain'),
                    'left' => __('Left', 'rt_domain'),
                    'right' => __('Right', 'rt_domain'),
                ),
            ));
        }

        if (rt_is_woocommerce()) {
            $this->add_field(array(
                'type' => 'radio-buttonset',
                'settings' => $settings . '_woocommerce_archive',
                'label' => __('Sidebar On Shop', 'rt_domain'),
                'section' => $section,
                'default' => 'right',
                'choices' => array(
                    'none' => __('No Sidebar', 'rt_domain'),
                    'left' => __('Left', 'rt_domain'),
                    'right' => __('Right', 'rt_domain'),
                ),
            ));

            $this->add_field(array(
                'type' => 'radio-buttonset',
                'settings' => $settings . '_woocommerce_single',
                'label' => __('Sidebar On Product', 'rt_domain'),
                'section' => $section,
                'default' => 'none',
                'choices' => array(
                    'none' => __('No Sidebar', 'rt_domain'),
                    'left' => __('Left', 'rt_domain'),
                    'right' => __('Right', 'rt_domain'),
                ),
            ));

        }
    }

    // end class
}

new Sidebar;
