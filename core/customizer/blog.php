<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Blog extends Customizer_Base
{
    public function __construct()
    {
        $this->set_panel();
        $this->set_section();

        // archive section
        $this->add_blog_options();
        $this->add_blog_element();
        $this->add_blog_meta();

        // single section
        $this->add_single_element();
        $this->add_single_meta();
        $this->add_single_related();

        // element
        $this->add_element_label();

    }

    public function set_panel()
    {
        $this->add_panel('blog_panel', array(
            'title' => __('Blog', 'rt_domain'),
        ));
    }

    public function set_section()
    {
        $this->add_section('blog_panel', array(
            'blog_archive' => array(esc_attr__('Archive', 'rt_domain')),
            'blog_single' => array(esc_attr__('Single', 'rt_domain')),
            'blog_label' => array(esc_attr__('Label', 'rt_domain')),
            'blog_readmore' => array(esc_attr__('Read More Button', 'rt_domain')),
        ));
    }

    public function add_blog_options()
    {
        $this->add_header(array(
            'label' => 'Options',
            'settings' => 'blog_options',
            'section' => 'blog_archive_section',
            'class' => 'blog_archive',

        ));


        $this->add_field_responsive(array(
            'type' => 'select',
            'section' => 'blog_archive_section',
            'class' => 'blog_archive',
            'settings' => 'blog_options_column',
            'label' => __('Column', 'rt_domain'),
            'default' => 3,
            'multiple' => 1,
            'choices' => array(
                1 => __('1 Column', 'rt_domain'),
                2 => __('2 Column', 'rt_domain'),
                3 => __('3 Column', 'rt_domain'),
                4 => __('4 Column', 'rt_domain'),
                6 => __('6 Column', 'rt_domain'),
            ),
            'active_callback' => array(
                array(
                    'setting' => 'blog_options_layout',
                    'operator' => 'in',
                    'value' => array('grid'),
                ),
            ),

        ));

        $this->add_field_responsive(array(
            'type' => 'slider',
            'settings' => 'blog_options_column_gap',
            'class' => 'blog_archive',
            'label' => __('Column Gap', 'rt_domain'),
            'section' => 'blog_archive_section',
            'default' => 30,
            'choices' => array(
                'min' => 0,
                'max' => 30,
            ),
            'output' => array(
                array(
                    'element' => '.rt-post-archive',
                    'property' => 'margin-left',
                    'value_pattern' => 'calc(-$px/2)',
                ),
                array(
                    'element' => '.rt-post-archive',
                    'property' => 'margin-right',
                    'value_pattern' => 'calc(-$px/2)',
                ),
                array(
                    'element' => '.rt-post-archive .flex-item',
                    'property' => 'padding-left',
                    'value_pattern' => 'calc($px/2)',
                ),
                array(
                    'element' => '.rt-post-archive .flex-item',
                    'property' => 'padding-right',
                    'value_pattern' => 'calc($px/2)',
                ),
                array(
                    'element' => '.rt-post-archive .flex-item',
                    'units' => 'px',
                    'property' => 'margin-bottom',
                ),
            ),
            'transport' => 'auto',
            'active_callback' => array(
                array(
                    'setting' => 'blog_options_layout',
                    'operator' => 'in',
                    'value' => 'grid',
                ),
            ),

        ));
 
        $this->add_field(array(
            'type' => 'number',
            'section' => 'blog_archive_section',
            'class' => 'blog_archive',
            'settings' => 'blog_options_excerpt',
            'label' => __('Excerpt', 'rt_domain'),
            'description' => 'Control how much the amount of text displayed',
            'default' => 18,
            'choices' => array(
                'min' => 0,
                'max' => 500,
            ),

        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'blog_options_image_size',
            'label' => __('Image Size', 'rt_domain'),
            'section' => 'blog_archive_section',
            'default' => 'featured_medium',
            'multiple' => 1,
            'choices' => get_intermediate_image_sizes(),
        ));

  
    }

    public function add_blog_element()
    {
        $this->add_header(array(
            'label' => 'Element',
            'settings' => 'blog_element',
            'section' => 'blog_archive_section',
            'class' => 'blog_element',
        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'blog_element_title',
            'label' => __('Title', 'rt_domain'),
            'section' => 'blog_archive_section',
            'class' => 'blog_element',
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'blog_element_thumbnail',
            'label' => __('Featured Image', 'rt_domain'),
            'section' => 'blog_archive_section',
            'class' => 'blog_element',
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'blog_element_meta',
            'label' => __('Meta', 'rt_domain'),
            'section' => 'blog_archive_section',
            'class' => 'blog_element',
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'blog_element_content',
            'label' => __('Content', 'rt_domain'),
            'section' => 'blog_archive_section',
            'class' => 'blog_element',
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'blog_element_readmore',
            'label' => __('Readmore', 'rt_domain'),
            'section' => 'blog_archive_section',
            'class' => 'blog_element',
            'default' => true,
        ));
    }

    public function add_blog_meta()
    {
        $this->add_header(array(
            'label' => 'Meta',
            'settings' => 'blog_meta',
            'section' => 'blog_archive_section',
            'class' => 'blog_meta',
        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'blog_meta_author',
            'label' => __('Author', 'rt_domain'),
            'section' => 'blog_archive_section',
            'class' => 'blog_meta',
            'default' => false,
        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'blog_meta_date',
            'label' => __('Date', 'rt_domain'),
            'section' => 'blog_archive_section',
            'class' => 'blog_meta',
            'default' => true,
        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'blog_meta_category',
            'label' => __('Category', 'rt_domain'),
            'section' => 'blog_archive_section',
            'class' => 'blog_meta',
            'default' => true,
        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'blog_meta_comment',
            'label' => __('Comment', 'rt_domain'),
            'section' => 'blog_archive_section',
            'class' => 'blog_meta',
            'default' => false,
        ));

    }

    public function add_single_element()
    {
        $this->add_header(array(
            'label' => 'Element',
            'settings' => 'single_element',
            'section' => 'blog_single_section',
            'class' => 'single_element',
        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_element_title',
            'label' => __('Title', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_element',
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_element_thumbnail',
            'label' => __('Featured Image', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_element',
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_element_content',
            'label' => __('Content', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_element',
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_element_tags',
            'label' => __('Tags', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_element',
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_element_author_info',
            'label' => __('Author Box', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_element',
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_element_related',
            'label' => __('Related Posts', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_element',
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_element_navigation',
            'label' => __('Navigation', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_element',
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_element_comment',
            'label' => __('Comment', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_element',
            'default' => true,
        ));
    }

    public function add_single_meta()
    {
        $this->add_header(array(
            'label' => 'Meta',
            'settings' => 'single_meta',
            'section' => 'blog_single_section',
            'class' => 'single_meta',
        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_meta_author',
            'label' => __('Author', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_meta',
            'default' => true,
        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_meta_date',
            'label' => __('Date', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_meta',
            'default' => true,
        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_meta_category',
            'label' => __('Category', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_meta',
            'default' => true,
        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'single_meta_comment',
            'label' => __('Comment', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_meta',
            'default' => true,
        ));

    }

    public function add_single_related()
    {
        $this->add_header(array(
            'label' => 'Related',
            'settings' => 'single_related',
            'section' => 'blog_single_section',
            'class' => 'single_related',
        ));

        $this->add_field(array(
            'type' => 'number',
            'settings' => 'single_related_count',
            'label' => __('Related Count', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_related',
            'default' => 6,
        ));

        $this->add_field_responsive(array(
            'type' => 'select',
            'settings' => 'single_related_show',
            'label' => __('Slider Show', 'rt_domain'),
            'section' => 'blog_single_section',
            'class' => 'single_related',
            'default' => 3,
            'multiple' => 1,
            'choices' => array(
                1 => __('1', 'rt_domain'),
                2 => __('2', 'rt_domain'),
                3 => __('3', 'rt_domain'),
                4 => __('4', 'rt_domain'),
                6 => __('6', 'rt_domain'),
            ),
        ));
    }

    /**
     * Blog element
     */
    public function add_element_label()
    {
       
    }

    // end class
}

new Blog;
