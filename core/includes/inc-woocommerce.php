<?php

/*=================================================
/* GET ARCHIVE ALL PRODUCT
/*=================================================
 * @desc This function detect all archive on post type product like category, tag, archive default
 * @return boolean
 */
function rt_is_product_archive()
{
    if (is_tax(array('product_cat', 'product_tag')) || rt_is_woocommerce('shop')) {
        return true;
    }
}

/*=================================================
 *  WOO COMMERCE SUPPORT
/*================================================= */
function rt_woocommerce_support()
{
    add_theme_support('woocommerce');
    add_theme_support('wc-product-gallery-zoom');
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-slider');
}
add_action('after_setup_theme', 'rt_woocommerce_support');

/*=================================================
 *   REMOVE TITLE SHOP
/*================================================= */
/** remove title shop on header page */
function rt_woocommerce_hide_page_title()
{
    return false;
}
add_filter('woocommerce_show_page_title', 'rt_woocommerce_hide_page_title');

/*=================================================;
/* ADD FIELD CLASS
/*=================================================
 * Added custom css class on woocommerce field
 * this bug WooCommerce 3.6.0
 */
function rt_input_css_classes($args, $key, $value = null)
{
    $class = 'rt-form';

    if (rt_is_woocommerce('checkout') && is_page_template('templates/template-focus.php')) {
        $class = 'rt-form js-form-overlay';
        if ($args['type'] == 'select') {
            $args['class'][] = "is-active";
        }

    }

    $args['class'][0] = $class;
    $args['label_class'][0] = 'rt-form__label';
    $args['input_class'][0] = 'rt-form__input';

    return $args;

}
add_filter('woocommerce_form_field_args', 'rt_input_css_classes', 10, 3);

/*=================================================;
/* ADD CLASS WRAPPER LOOP
/*================================================= */

function rt_woocommerce_class_wrapper($classes)
{
    $classes[] = 'rt-product-archive products flex flex-row flex-loop';

    /* Column */
    $column = 12 / rt_option('woocommerce_archive_options_column', 3);
    $column_md = 12 / rt_option('woocommerce_archive_options_column_tablet', 2);
    $column_sm = 12 / rt_option('woocommerce_archive_options_column_mobile', 2);

    $classes[] = 'flex-cols-lg-' . $column;
    $classes[] = 'flex-cols-md-' . $column_md;
    $classes[] = 'flex-cols-xs-' . $column_sm;

    if (rt_option('woocommerce_archive_masonry', false)) {
        $classes[] = 'js-masonry';
    }

    return $classes;

}
add_filter('rt_product_class_wrapper', 'rt_woocommerce_class_wrapper');

/*=================================================
 *  CHANGE LOOP PERPAGE
/*================================================= */
function rt_woocommerce_per_page($loop)
{
    $post_per_page = rt_option('woocommerce_archive_options_post_per_page', 9);

    if (rt_is_only_tablet()) {
        $post_per_page = rt_option('woocommerce_archive_options_post_per_page_tablet', 8);
    }

    if (rt_is_only_mobile()) {
        $post_per_page = rt_option('woocommerce_archive_options_post_per_page_mobile', 8);
    }
    return $post_per_page;
};

add_filter('loop_shop_per_page', 'rt_woocommerce_per_page', 10, 1);

/*=================================================;
/* PRODUCT AJAX QUERY
/*================================================= */
function rt_woocommerce_query_loop($loop)
{
    $post_per_page = (rt_is_mobile()) ? rt_option('woocommerce_archive_options_post_per_page_tablet', 8) : rt_option('woocommerce_archive_options_post_per_page', 9);

    if (rt_is_product_archive()) {
        $loop = array(
            'post_type' => 'product',
            'posts_per_page' => $post_per_page,
            'template_part' => 'woocommerce/content-product',
            'pagination' => rt_option('woocommerce_archive_options_pagination', 'number'),
        );

    }
    return $loop;
};

add_filter('rt_loop_query', 'rt_woocommerce_query_loop');

/*=================================================
 *  REMOVE WOO PAGINATION
/*================================================= */
remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);

/*=================================================;
/* WOOCOMMERCE ARCHIVE
/*=================================================
 * @desc Remove theme DOM wraper loop <div id="post_archive"></div>
 */
function rt_woocommerce_wrapper_post()
{
    if (rt_is_product_archive()) {
        remove_action('rt_before_loop', 'rt_before_loop', 5);
        remove_action('rt_after_loop', 'rt_after_loop', 5);
    }

}

add_action('wp', 'rt_woocommerce_wrapper_post');

/*=================================================;
/* ARCHIVE - FILTER
/*=================================================
 * wrap woocommerce before loop element
 * @see woocommerce archive hook
 */
function rt_shop_result_count()
{
    echo '<div class="rt-shop-filter-wrapper">';
}
add_action('woocommerce_before_shop_loop', 'rt_shop_result_count', 19);

function rt_shop_catalog_ordering()
{
    echo '</div>';
}
add_action('woocommerce_before_shop_loop', 'rt_shop_catalog_ordering', 31);

/*=================================================
 * HOOK ARCHIVE
/*=================================================
 * remove add to cart button on archive product
 */
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

/*=================================================;
/* ARCHIVE - REMOVE DESCRIPTION
/*================================================= */
remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
remove_action('woocommerce_archive_description', 'woocommerce_product_archive_description', 10);

/*=================================================;
/* ADD TO CART BUTTON ON ARCHIVE
/*================================================= */
function rt_add_to_cart_loop_button()
{
    // Yith Label
    $label = get_option('yith-wcqv-button-label');
    $label = call_user_func('__', $label, 'yith-woocommerce-quick-view');
    $label = apply_filters('yith_wcqv_button_label', esc_html($label));

    // compatible with yith quick view
    if (!class_exists('YITH_WCQV')) {
        woocommerce_template_loop_add_to_cart();
    } else {
        echo '<a href="#" class="button yith-wcqv-button rt-product__btn" data-product_id="' . get_the_ID() . '">' . $label . '</a>';
    }
}
add_action('woocommerce_before_shop_loop_item_title', 'rt_add_to_cart_loop_button', 10);

/*=================================================
 * CART - SIDE PANEL
/*=================================================
 * added sidepannel on global footer
 */
function rt_woocommerce_cart_side_panel()
{
    if (rt_option('woocommerce_ajax_mini_cart', true)) {
        rt_get_template_part('shop/panel-cart');
    }
}
add_action('rt_footer', 'rt_woocommerce_cart_side_panel');

/*=================================================;
/* USER - MODAL LOGIN
/*================================================= */
function rt_woocommerce_user_modal()
{
    rt_get_template_part('shop/form-login-modal');

}
add_action('rt_footer', 'rt_woocommerce_user_modal');

/*=================================================;
/* IMAGE SIZE
/*=================================================
 * @desc Use this filter image if woocommerce archive active */

function rt_woocommerce_image_size($size)
{
    if (rt_option('woocommerce_archive_masonry', false)) {
        $size = array(
            'width' => 400,
            'height' => 999999999,
            'crop' => 1,
        );
    }

    return $size;

}
add_filter('wc_get_image_size', 'rt_woocommerce_image_size');

/*=================================================
 *  DISABLE ADD TO CART
/*================================================= */
if (rt_option('woocommerce_catalog', false)) {
    add_filter('woocommerce_is_purchasable', '__return_false');
}

/*=================================================
 * SHARE
/*================================================= */
/** add share button woocommerce single */
function rt_woocommerce_share()
{
    if (rt_option('woocommerce_single_share', true)) {
        rt_get_template_part('global/share');
    }

}

add_action('woocommerce_share', 'rt_woocommerce_share');

/*=================================================
 *  COUNT WOO
/*================================================= */
/** Show cart count and number if ajax add to cart on click */
function rt_woocommerce_header_add_to_cart_fragment($fragments)
{
    global $woocommerce;

    ob_start();

    echo '<span class="rt-header__cart-count js-cart-total">' . $woocommerce->cart->cart_contents_count . '</span>';

    $fragments['.js-cart-total'] = ob_get_clean();

    return $fragments;

}

add_filter('woocommerce_add_to_cart_fragments', 'rt_woocommerce_header_add_to_cart_fragment');

/*=================================================
 *  AJAX CART
/*=================================================
 * @desc add script ajax woocomerce single page */

function rt_woocommerce_ajax_cart_scripts()
{
    if (rt_option('woocommerce_ajax_mini_cart', true)) {
        wp_enqueue_script('retheme_cart_ajax', get_template_directory_uri() . '/assets/js/add-to-cart.min.js', array('jquery'), false, true);
    }

}
add_action('wp_enqueue_scripts', 'rt_woocommerce_ajax_cart_scripts');

function rt_woocommerce_add_cart_single_ajax()
{
    if (rt_option('woocommerce_ajax_mini_cart', true)) {
  

    }
}

add_action('wp_ajax_retheme_add_cart_single', 'rt_woocommerce_add_cart_single_ajax');
add_action('wp_ajax_nopriv_retheme_add_cart_single', 'rt_woocommerce_add_cart_single_ajax');

/*=================================================;
/* PRODUCT REVIEW FIELD
/*================================================= */
function rt_product_review_comment_form()
{
    $commenter = wp_get_current_commenter();

    return array(
        'fields' => array(
            'author' => '<div class="comment-wrapper-personal"><p class="comment-form-author rt-form ">' . '<label class="rt-form__label" for="author">' . esc_html__('Name', 'woocommerce') . '&nbsp;<span class="required">*</span></label> ' .
            '<input id="author" class="rt-form__input" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30" required /></p>',
            'email' => '<p class="comment-form-email rt-form "><label class="rt-form__label" for="email">' . esc_html__('Email', 'woocommerce') . '&nbsp;<span class="required">*</span></label> ' .
            '<input id="email" class="rt-form__input" name="email" type="email" value="' . esc_attr($commenter['comment_author_email']) . '" size="30" required /></p></div>',
        ),
        'comment_field' => '<p class="comment-form-comment rt-form ""><label class="rt-form__label" for="comment">' . esc_html__('Your review', 'woocommerce') . '&nbsp;<span class="required">*</span></label><textarea id="comment" class="rt-form__input" name="comment" cols="45" rows="8" required></textarea></p>',

    );

}
add_filter('woocommerce_product_review_comment_form_args', 'rt_product_review_comment_form');

/*=================================================;
/* PRODUCT - REMOVE TITLE TAB
/*=================================================
 * remove title header from tab heading
 */
function rt_product_tabs_heading()
{
    return false;
}
add_action('woocommerce_product_description_heading', 'rt_product_tabs_heading');
add_action('woocommerce_product_additional_information_heading', 'rt_product_tabs_heading');

/*=================================================;
/* SINGLE LAYOUT
/*================================================= */
function rt_product_layout($classes)
{



    // hidden add to cart button with css
    if (rt_option('woocommerce_catalog', false)) {
        $classes[] = 'catalog-mode';
    }

    return $classes;
}

add_filter('body_class', 'rt_product_layout');



/*=================================================;
/* MOVE CROSS SELLING
/*================================================= */
remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
add_action('woocommerce_after_cart_table', 'woocommerce_cross_sell_display');

/*=================================================;
/* PRODUCT - REMOVE META
/*================================================= */
function rt_product_remove_meta()
{
    if (!rt_option('woocommerce_single_meta', true)) {
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
    }

}
add_action('after_setup_theme', 'rt_product_remove_meta');

/*=================================================;
/* CHECKOUT - LINK SHOP
/*=================================================
 * add link return shop on template focus
 */

function rt_checkout_link_shop()
{
    if (is_page_template('templates/template-focus.php')) {
        echo '<div class="text-center">';
        echo '<a class="rt-return-shop" href="' . get_permalink(wc_get_page_id('cart')) . '">
        <i class="fa fa-chevron-circle-left mr-5"></i>
' . __('Return to Cart', 'rt_domain') . '</a>';
        echo '</div>';
    }
}
add_action('woocommerce_checkout_after_order_review', 'rt_checkout_link_shop');



/*=================================================;
/* GLOBALS - REMOVE DEFAULT STYLE
/*================================================= */
function rt_woocommerce_dequeue_styles($enqueue_styles)
{
    unset($enqueue_styles['woocommerce-general']); // Remove the gloss
    return $enqueue_styles;
}
add_filter('woocommerce_enqueue_styles', 'rt_woocommerce_dequeue_styles');

/*=================================================;
/* SHOP - REMOVE PREFIX PAGE TITLE
/*================================================= */
function rt_shop_remove_prefix_title($title)
{
    if (rt_is_woocommerce('category')) {
        $title = single_cat_title('', false);
    } elseif (rt_is_woocommerce('tag')) {
        $title = single_cat_title('', false);
    }
    return $title;

}
add_filter('get_the_archive_title', 'rt_shop_remove_prefix_title');