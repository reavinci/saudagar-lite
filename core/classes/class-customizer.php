<?php
namespace Retheme;

use Retheme\Helper;

/**
 * @author Reret Yosyanto hello@rereyossi.com
 * @required - Class Hellper, Kirki Library
 * @desc - function library for kirki customizer
 */
class Customizer_Base
{

    public $breakpoint_large = '@media (min-width: 960px)';
    public $breakpoint_medium = '@media (max-width: 768px)';
    public $breakpoint_small = '@media (max-width: 468px)';

    public function __construct()
    {
        $this->add_config();
    }

    /**
     * Add customizer options
     */
    public function add_config()
    {
        \Kirki::add_config('retheme_customizer', array(
            'capability' => 'edit_theme_options',
            'option_type' => 'theme_mod',
        ));
    }

    /**
     * Add panel
     * required kirki
     */
    public function add_panel($name, $args)
    {
        \Kirki::add_panel($name, $args);
    }

    /**
     * Add section
     * required kirki
     */
    public function add_section($panel = '', $sections)
    {
        if (rt_is_free()) {
            \Kirki::add_section(
                'theme_pro',
                [
                    'title' => esc_html__('More Options', 'rt_domain'),
                    'type' => 'link',
                    'button_text' => 'Get '.rt_var('product-name').' Pro',
                    'button_url' => rt_var('product-url'),
                    'priority' => 1,
                ]
            );

        }

        foreach ($sections as $section_id => $section) {
            if (!empty($section[0])) {
                $section_args['title'] = $section[0];
            }

            if (!empty($section[1])) {
                $section_args['description'] = $section[1];
            }

            if (!empty($panel)) {
                $section_args['panel'] = $panel;
            }

            if (!empty($section[2])) {
                $section_args['type'] = $section[2];
            }

            \Kirki::add_section($section_id . '_section', $section_args);

        }
    }

    /**
     * Add field control
     */
    public function add_field($args)
    {
        \Kirki::add_field('retheme_customizer', $args);
    }

    /**
     * Add header control
     */
    public function add_header($args = array())
    {
        $child = '';

        $tooltip = !empty($args['tooltip']) ? $args['tooltip'] : '';

        if (!empty($args['child'])) {
            foreach ($args['child'] as $key => $value) {
                $data[] = '#customize-control-' . $value;
            }
            $child = join(',', $data);
        }

        $this->add_field(array(
            'type' => 'custom',
            'settings' => 'retheme_header_' . $args['settings'],
            'section' => $args['section'],
            'class' => !empty($args['class']) ? $args['class'] : '',
            'default' => '<div class="retheme-header" data-child="' . $child . '">' . $args['label'] . '</div>',
            'active_callback' => (!empty($args['active_callback'])) ? $args['active_callback'] : '',
            'tooltip' => $tooltip,
        ));
    }

    public function add_sub_header($args = array())
    {
        $this->add_field(array(
            'type' => 'custom',
            'settings' => 'retheme_sub_header_' . $args['settings'],
            'section' => $args['section'],
            'class' => !empty($args['class']) ? $args['class'] : '',
            'default' => '<div class="retheme_subheader">' . $args['label'] . '</div>',
        ));
    }

    /**
     * Responsive control with mobile choose
     * @desc - Add tablet and mobile control
     * @param $args - argument from control
     * @return HTML - control field
     */
    public function add_field_responsive($defaults = array())
    {

        /** Add media query each element output */
        $label = !empty($defaults['label']) ? $defaults['label'] : 'Typography';

        /** Add media query each element output */
        if (!empty($defaults['output'])) {

            foreach ($defaults['output'] as $key => $value) {
                $output_tablet[] = wp_parse_args(['media_query' => $this->breakpoint_medium], $value);
                $output_mobile[] = wp_parse_args(['media_query' => $this->breakpoint_small], $value);
            }

        } elseif (!empty($args['element'])) {
            $output_tablet[] = array('media_query' => $this->breakpoint_medium, 'element' => $defaults['element']);
            $output_mobile[] = array('media_query' => $this->breakpoint_small, 'element' => $defaults['element']);
        } else {
            $output_tablet = '';
            $output_mobile = '';

        }

        /** Show responsive control */
        $this->add_field(wp_parse_args(array(
            'device' => 'desktop',
        ), $defaults));

        $this->add_field(wp_parse_args(array(
            'settings' => $defaults['settings'] . '_tablet',
            'device' => 'tablet',
            'output' => $output_tablet,
        ), $defaults));

        $this->add_field(wp_parse_args(array(
            'settings' => $defaults['settings'] . '_mobile',
            'device' => 'mobile',
            'output' => $output_mobile,
        ), $defaults));

    }

    /**
     * Background Group Control
     *
     * @param array $args
     * @return void
     */
    public function add_field_background($args = array())
    {
        $label = !empty($args['label']) ? $args['label'] : 'Background Color';

        $this->add_field(wp_parse_args(array(
            'label' => __($label, 'rt_domain'),
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'output' => array(
                array(
                    'element' => $args['element'],
                    'property' => 'background-color',
                ),
            ),
            'transport' => 'auto',
        ), $args));

        if (!empty($args['pseudo'])) {
            $this->add_field(wp_parse_args(array(
                'label' => __($label . ' :Hover', 'rt_domain'),
                'type' => 'color',
                'settings' => $args['settings'] . '_hover',
                'choices' => array(
                    'alpha' => true,
                ),
                'output' => $this->selector($args['element'], 'background-color', $args['pseudo']),
                'transport' => 'auto',
            ), $args));
        }

    }
    /**
     * Color Group Control
     *
     * @param array $args
     * @return void
     */
    public function add_field_color($args = array())
    {
        $label = !empty($args['label']) ? $args['label'] : 'Color';

        $this->add_field(wp_parse_args(array(
            'label' => __($label, 'rt_domain'),
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'output' => array(
                array(
                    'element' => $args['element'],
                    'property' => 'color',
                ),
            ),
            'transport' => 'auto',
        ), $args));

        if (!empty($args['pseudo'])) {
            $this->add_field(wp_parse_args(array(
                'label' => __($label . ' :Hover', 'rt_domain'),
                'type' => 'color',
                'settings' => $args['settings'] . '_hover',
                'choices' => array(
                    'alpha' => true,
                ),
                'output' => $this->selector($args['element'], 'color', $args['pseudo']),
                'transport' => 'auto',
            ), $args));
        }
    }

    /**
     * Link Group Control
     *
     * @param array $args
     * @return void
     */
    public function add_field_link($args = array())
    {
        $label = !empty($args['label']) ? $args['label'] : 'Link';

        $this->add_field(wp_parse_args(array(
            'label' => __($label, 'rt_domain'),
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'output' => array(
                array(
                    'element' => $args['element'],
                    'property' => 'color',
                ),
            ),
            'transport' => 'auto',
        ), $args));

        if (!empty($args['pseudo'])) {
            $this->add_field(wp_parse_args(array(
                'label' => __($label . ':Hover', 'rt_domain'),
                'type' => 'color',
                'settings' => $args['settings'] . '_hover',
                'choices' => array(
                    'alpha' => true,
                ),
                'output' => $this->selector($args['element'], 'color', $args['pseudo']),
                'transport' => 'auto',
            ), $args));
        }
    }

    /**
     * Border Color Control
     *
     * @param array $args
     * @return void
     */
    public function add_field_border_color($args = array())
    {
        $label = !empty($args['label']) ? $args['label'] : 'Border Color';

        $this->add_field(wp_parse_args(array(
            'label' => __($label, 'rt_domain'),
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'output' => array(
                array(
                    'element' => $args['element'],
                    'property' => 'border-color',
                ),
            ),
            'transport' => 'auto',
        ), $args));

        if (!empty($args['pseudo'])) {
            $this->add_field(wp_parse_args(array(
                'label' => __($label . ' :Hover', 'rt_domain'),
                'type' => 'color',
                'settings' => $args['settings'] . '_hover',
                'choices' => array(
                    'alpha' => true,
                ),
                'output' => $this->selector($args['element'], 'border-color', $args['pseudo']),
                'transport' => 'auto',
            ), $args));
        }
    }

    /**
     * Border Radius Control
     *
     * @param array $args
     * @return void
     */
    public function add_field_border_radius($args = array())
    {
        $label = !empty($args['label']) ? $args['label'] : 'Border Radius';
        /**
         * Merge default array with array from control
         * @param array $args, $default
         */
        $this->add_field(wp_parse_args(array(
            'label' => __($label, 'rt_domain'),
            'type' => 'slider',
            'choices' => array(
                'min' => '0',
                'max' => '100',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => $args['element'],
                    'property' => 'border-radius',
                    'units' => 'px',
                ),
            ),
            'transport' => 'auto',
        ), $args));
    }

    /**
     * Animation Control
     *
     * @param array $args
     * @return void
     */
    public function add_field_animation($args = array())
    {
        /**
         * Merge default array with array from control
         * @param array $args, $default
         */
        $this->add_field(wp_parse_args(array(
            'type' => 'select',
            'label' => __('Animation', 'admin_domain'),
            'settings' => $args['settings'],
            'default' => 'transition.fadeIn',
            'choices' => Helper::get_animation_in(),
        ), $args));

        $this->add_field(wp_parse_args(array(
            'type' => 'number',
            'label' => __('Duration', 'rt_domain'),
            'settings' => $args['settings'] . '_duration',
            'default' => 300,
            'choices' => array(
                'min' => 120,
                'max' => 1000,
            ),
        ), $args));

    }

    /*
     * Button group
     */
    public function add_field_button($args = array())
    {
        $class = !empty($args['class']) ? $args['class'] : '';

        $this->add_field_color(array(
            'settings' => $args['settings'] . '_color',
            'section' => $args['section'],
            'class' => $class,
            'element' => $args['element'],
            'pseudo' => 'all',
        ));

        $this->add_field_background(array(
            'settings' => $args['settings'] . '_background',
            'section' => $args['section'],
            'class' => $class,
            'element' => $args['element'],
            'pseudo' => 'all',
        ));

        $this->add_field_border_color(array(
            'settings' => $args['settings'] . '_border_color',
            'section' => $args['section'],
            'class' => $class,
            'element' => $args['element'],
            'pseudo' => 'all',
        ));

        $this->add_field_border_radius(array(
            'settings' => $args['settings'] . '_border_radius',
            'section' => $args['section'],
            'class' => $class,
            'element' => $args['element'],
            'pseudo' => 'all',
        ));
    }

    /*
     * Form group
     */
    public function add_field_form($args = array())
    {

        $class = !empty($args['class']) ? $args['class'] : '';

        $this->add_field_color(array(
            'label' => 'Placeholder Color',
            'settings' => $args['settings'] . '_color',
            'section' => $args['section'],
            'class' => $class,
            'element' => $args['element'],
            'pseudo' => 'hover',
        ));

        $this->add_field_background(array(
            'settings' => $args['settings'] . '_background',
            'section' => $args['section'],
            'class' => $class,
            'element' => $args['element'],
            'pseudo' => 'hover',
        ));

        $this->add_field_border_color(array(
            'settings' => $args['settings'] . '_border_color',
            'section' => $args['section'],
            'class' => $class,
            'element' => $args['element'],
            'pseudo' => 'hover',
        ));
        $this->add_field_border_radius(array(
            'settings' => $args['settings'] . '_border_radius',
            'section' => $args['section'],
            'class' => $class,
            'element' => $args['element'],
        ));
    }

    /**
     * Desc
     */
    public function add_desc($args)
    {
        $this->add_field(array(
            'type' => 'custom',
            'settings' => $args['settings'],
            'section' => $args['section'],
            'default' => '<div class="retheme-desc">' . $args['label'] . '</div>',
        ));
    }

    /*
     * Merge selector for hover
     */
    public function selector($selector, $property, $pseudo = '')
    {
        $data = explode(",", $selector);

        foreach ($data as $key => $value) {

            if ($pseudo === 'hover') {
                $element[$key]['element'] = $value . ':hover';
            }

            if ($pseudo === 'all') {
                $element[$key]['element'] = $value . ':hover, ' . $value . ':active, ' . $value . ':focus';
            }

            $element[$key]['property'] = $property;
        }
        return $element;
    }

    public function testing()
    {
        return ['satu', 'dua', 'tiga'];
    }

    // end class
}

new Customizer_Base;
