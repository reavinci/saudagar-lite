<div class="bulma">

  <div class="tabs rta-theme-menu">
    <ul>
      <li class="<?php echo (rt_admin_get_page() == 'theme-panel')? 'is-active':''?>"><a href="<?php echo rt_admin_page_url('theme-panel')?>">Dashboard</a></li>

      <?php if(rt_is_premium_plan()): ?>
      <li class="<?php echo (rt_admin_get_page() == 'theme-license')? 'is-active':''?>"><a href="<?php echo rt_admin_page_url('theme-license')?>">License</a></li>
      <?php endif ?>
      
      <?php if(class_exists('OCDI_Plugin')): ?>
      <li class="<?php echo (rt_admin_get_page() == 'pt-one-click-demo-import')? 'is-active':''?>"><a href="<?php echo rt_admin_page_url('pt-one-click-demo-import')?>">Import Demo</a></li>
      <?php endif ?>

      <?php if(rt_is_free()): ?>
      <li class="<?php echo (rt_admin_get_page() == 'theme-pro')? 'is-active':''?>"><a href="<?php echo rt_admin_page_url('theme-pro')?>"><span class="has-text-warning">Upgrade Pro <i class="fa fa-star"></i></span></a></li>
      <?php endif ?>

    </ul>
  </div>
</div>