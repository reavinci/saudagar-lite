<?php
$url = get_permalink();
$url = urlencode($url);
$style = rt_option('connect_share_style', 'border');
?>

<div class="rt-share mb-20">
    <div class="rt-socmed rt-socmed--text <?php echo 'rt-socmed--'.$style?>">

    <?php if (rt_option('connect_share_facebook', true)): ?>
    <a class="rt-socmed__item facebook" target="_blank" rel="nofollow" href="https://www.facebook.com/sharer/sharer.php?display=popup&u=<?php echo $url; ?>">
        <i class="fa fa-facebook-f"></i><?php _e('Share', 'rt_domain') ?>
    </a>
    <?php endif;?>

    <?php if (rt_option('connect_share_twitter', true)): ?>
    <a class="rt-socmed__item twitter" target="_blank" rel="nofollow" href="https://twitter.com/home?status=<?php echo wp_strip_all_tags(get_the_title()) ?>" url="<?php echo $url; ?>">
        <i class="fa fa-twitter"></i><?php _e('Tweet', 'rt_domain') ?>
    </a>
    <?php endif;?>

    <?php if (rt_option('connect_share_pinterest', true)): ?>
    <a class="rt-socmed__item pinterest" target="_blank" rel="nofollow" href="https://pinterest.com/pin/create/bookmarklet/?media=<?php the_post_thumbnail_url();?>&amp;url=<?php echo $url; ?>">
        <i class="fa fa-pinterest"></i><?php _e('Pin', 'rt_domain') ?>
    </a>
    <?php endif;?>

    <?php if (rt_option('connect_share_email', true)): ?>
    <a class="rt-socmed__item email" href="mailto:?subject=<?php echo wp_strip_all_tags(get_the_title()) ?>" body="<?php echo $url ?>" title="<?php echo wp_strip_all_tags(get_the_title()) ?>">
        <i class="fa fa-envelope"></i><?php _e('Send', 'rt_domain') ?>
    </a>
    <?php endif; ?>

    </div>
</div>