<?php
$classes[] = 'rt-menu rt-menu--vertical js-menu';
?>

<div  <?php rt_set_class('rt_mobile_menu_class', $classes)?>>

<?php
if (has_nav_menu('primary')) {
    $args = array(
        'container' =>  '',
        'menu_class' => 'rt-menu__main',
        'theme_location' => 'primary',
    );

    wp_nav_menu($args);

}
?>

</div>
