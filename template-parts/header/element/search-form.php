<?php
$classes[] = 'rt-header__element rt-header__search-form rt-search';
 ?>
 <div  <?php rt_set_class('rt-header__search_class', $classes);?>>
    <form class="rt-search__inner" action="<?php echo esc_url(home_url('/')) ?>" id="search-form" >
    <input class="rt-search__input" type="text" placeholder="<?php echo rt_option('search_options_text', 'Type Something and enter') ?>
    " name="s" id="s">
            <button type="submit" class="rt-search__btn"><i class="ti-search"></i></button>

             <input type="hidden" name="post_type" value="<?php echo rt_option('search_options_result', 'post')?>" />
    </form>
 </div>
