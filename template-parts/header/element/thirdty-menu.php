<?php
$classes[] = 'rt-menu rt-menu--horizontal js-menu';
// $classes[] = 'rt-menu--'.rt_option('header_main_menu_effect', 'effect-line');
?>

<div  <?php rt_set_class('rt_thirty_menu_class', $classes) ?>
data-animatein='<?php echo rt_option('header_main_submenu_animation_in', 'transition.fadeIn') ?>'
data-duration='<?php echo rt_option('header_main_submenu_animation_duration', '300') ?>'>

  <?php 
  if(has_nav_menu('thirty')){
    $args = array(
    'container'      => '',
    'menu_class'     => 'rt-menu__main',
    'theme_location' => 'thirty',
  );
  wp_nav_menu($args);
  }
?>

</div>
