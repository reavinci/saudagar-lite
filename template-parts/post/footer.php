<div class="rt-post__footer">
  <a href="<?php the_permalink() ?>" class="rt-post__readmore">
    <?php echo rt_option('blog_readmore_text','Read More') ?>
  </a>
</div>
