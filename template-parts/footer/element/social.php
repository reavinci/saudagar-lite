<?php if (rt_option('social_item')): ?>

  <div  class="rt-socmed rt-socmed---sm rt-socmed--simple">
    <?php foreach (rt_option('social_item') as $key => $value): ?>

      <a href="<?php echo $value['link_url'] ?>" class="rt-socmed__item <?php echo $value['link_text'] ?>">
            <i class="fa fa-<?php echo $value['link_text'] ?>"></i>
      </a>
    <?php endforeach; ?>

  </div>
<?php endif; ?>
