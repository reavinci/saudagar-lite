<?php
/**
 * Checkout coupon form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-coupon.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.4
 */

  /**
 * Retheme 1.0.0 - Overridden
 * To do:
 * - add panel form
 */


defined( 'ABSPATH' ) || exit;

if ( ! wc_coupons_enabled() ) { // @codingStandardsIgnoreLine.
	return;
}

?>
<div class="rt-modal rt-coupon-modal js-modal" 
data-trigger=".js-coupon-modal-trigger"
data-animatein='transition.expandIn'
data-animateout='transition.expandOut'
data-duration='300'>

  
	<div class="rt-modal__overlay js-modal-close"></div>

    <div class="rt-modal__inner">
		<div class="rt-modal__header">
			<h5 class="rt-modal__title"><?php _e('Coupon', 'rt_domain')?></h5>
			<i class="rt-modal__close js-modal-close ti-close"></i>
		</div>
        <div class="rt-modal__body">
            
		<form class="checkout_coupon woocommerce-form-coupon" method="post">

			<p><?php esc_html_e( 'If you have a coupon code, please apply it below.', 'woocommerce' ); ?></p>
			<div class="rt-form">
				<input type="text" name="coupon_code" class="input-text rt-form__input" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value="" />
				<button type="submit" class="rt-btn rt-btn--second" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_html_e( 'Apply coupon', 'woocommerce' ); ?></button>
			</div>
		</form>

        </div>

    </div>
</div>