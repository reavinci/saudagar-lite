<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

 /**
 * Retheme 1.0.1 - Overridden
 * To do:
 * - remove fieldset dom
 * - remove legend dom
 * -- added tabs
 */


defined('ABSPATH') || exit;

do_action('woocommerce_before_edit_account_form');?>

<form class="woocommerce-EditAccountForm edit-account mb-60" action="" method="post" <?php do_action('woocommerce_edit_account_form_tag');?> >

	<?php do_action('woocommerce_edit_account_form_start');?>



	<!-- tab start -->
	<div class="rt-tab rt-tab--full-nav js-tab mb-20" data-animatein="transition.fadeIn" data-duration='300'>
		<div class="rt-tab__nav">
			<li>
				<h5 href="#account-field" class="rt-tab__title is-active"><?php _e('Profile', 'rt_domain') ?></h5>
			</li>
			<li>
				<h5 href="#account-password" class="rt-tab__title"><?php _e('Password', 'rt_domain') ?></h5>
			</li>
		</div>

		<div class="rt-tab__body">
			<div id="account-field" class="rt-tab__item is-active">

				<!-- account detail -->
				<p class="rt-form woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
					<label class="rt-form__label" for="account_first_name"><?php esc_html_e('First name', 'woocommerce');?>&nbsp;<span class="required">*</span></label>
					<input type="text" class="rt-form__input rt-form__input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr($user->first_name); ?>" />
				</p>
				<p class="rt-form woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
					<label class="rt-form__label" for="account_last_name"><?php esc_html_e('Last name', 'woocommerce');?>&nbsp;<span class="required">*</span></label>
					<input type="text" class="rt-form__input rt-form__input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr($user->last_name); ?>" />
				</p>
				<div class="clear"></div>

				<p class="rt-form woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label class="rt-form__label" for="account_display_name"><?php esc_html_e('Display name', 'woocommerce');?>&nbsp;<span class="required">*</span></label>
					<input type="text" class="rt-form__input rt-form__input--text input-text" name="account_display_name" id="account_display_name" value="<?php echo esc_attr($user->display_name); ?>" /> <span style=""><em><?php esc_html_e('This will be how your name will be displayed in the account section and in reviews', 'woocommerce');?></em></span>
				</p>
				<div class="clear"></div>

				<p class="rt-form woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label class="rt-form__label" for="account_email"><?php esc_html_e('Email address', 'woocommerce');?>&nbsp;<span class="required">*</span></label>
					<input type="email" class="rt-form__input rt-form__input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr($user->user_email); ?>" />
				</p>

			</div>

			<div id="account-password" class="rt-tab__item">

				<p class="rt-alert rt-alert--warning"><?php _e('leave blank to leave unchanged', 'woocommerce') ?></p>

				<p class="rt-form woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label class="rt-form__label" for="password_current"><?php esc_html_e('Current password', 'woocommerce');?></label>
					<input type="password" class="rt-form__input rt-form__input--password input-text" name="password_current" id="password_current" autocomplete="off" />
				</p>
				<p class="rt-form woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label class="rt-form__label" for="password_1"><?php esc_html_e('New password', 'woocommerce');?></label>
					<input type="password" class="rt-form__input rt-form__input--password input-text" name="password_1" id="password_1" autocomplete="off" />
				</p>
				<p class="rt-form woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label class="rt-form__label" for="password_2"><?php esc_html_e('Confirm new password', 'woocommerce');?></label>
					<input type="password" class="rt-form__input rt-form__input--password input-text" name="password_2" id="password_2" autocomplete="off" />
				</p>

			</div>

		</div>

	
	</div>
	<!-- tab end -->


	<div class="clear"></div>

	<?php do_action('woocommerce_edit_account_form');?>

	<p>
		<?php wp_nonce_field('save_account_details', 'save-account-details-nonce');?>
		<button type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e('Save changes', 'woocommerce');?>"><?php esc_html_e('Save changes', 'woocommerce');?></button>
		<input type="hidden" name="action" value="save_account_details" />
	</p>
	

	<?php do_action('woocommerce_edit_account_form_end');?>
</form>

<?php do_action('woocommerce_after_edit_account_form');?>


