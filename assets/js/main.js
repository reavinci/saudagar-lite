"use strict";

jQuery(document).ready(function ($) {
  $.fn.retheme_accordion = function (options) {
    return this.each(function () {
      var element = $(this);
      var defaults = {
        animatein: 'transition.fadeIn',
        animateout: 'transition.fadeOut',
        duration: '300',
        easing: 'easeIn',
        toggle: ''
      };
      var meta = element.data();
      var options = $.extend(defaults, options, meta);
      var selector = {
        nav: '.rt-accordion__title',
        item: '.rt-accordion__content'
        /*
         * Event
         */

      };
      element.find(selector.nav).on('click', function (event) {
        event.preventDefault();
        /*
         * Close all Tabs
         */

        if (options.toggle) {
          element.find('.rt-accordion__item').removeClass('is-active');
          element.find(selector.item).slideUp(options.duration);
        }
        /** active tab menu */


        $(this).parent().addClass('is-active');
        /** Open tab content */

        $(this).parent().find(selector.item).slideDown(options.duration);
      });
    });
  };
});
"use strict";

jQuery(document).ready(function ($) {});
"use strict";

jQuery(document).ready(function ($) {
  /*=================================================;
  /* FORM CLICK
  /*================================================= */
  var form_overlay = function form_overlay() {
    var form = $('.js-form-overlay').find('.rt-form__input');
    form.each(function () {
      if ($(this).val() != '') {
        $(this).parents('.rt-form').addClass("is-active");
      }
    });
    form.on('focus', function () {
      form.each(function () {
        if ($(this).val() == '') {
          $(this).parents('.rt-form').removeClass("is-active");
        }
      });
      $(this).parents('.rt-form').addClass("is-active");
    });
  };

  form_overlay(); // end document
});
"use strict";

jQuery(document).ready(function ($) {
  $.fn.hasData = function (key) {
    return typeof $(this).data(key) != "undefined";
  };

  $.fn.getData = function (key) {
    var data = false;

    if ($(this).data(key) != "undefined") {
      var data = $(this).data(key);
    }

    return data;
  };
  /*=================================================
  * VELOCITY EASE
  /*================================================= */


  var velocity_easings = {
    easeIn: "ease-in",
    easeOut: "ease-out",
    easeInOut: "ease-in-out",
    snap: [0.0, 1.0, 0.5, 1.0],
    linear: [0.25, 0.25, 0.75, 0.75],
    easeInQuad: [0.55, 0.085, 0.68, 0.53],
    easeInCubic: [0.55, 0.055, 0.675, 0.19],
    easeInQuart: [0.895, 0.03, 0.685, 0.22],
    easeInQuint: [0.755, 0.05, 0.855, 0.06],
    easeInSine: [0.47, 0.0, 0.745, 0.715],
    easeInExpo: [0.95, 0.05, 0.795, 0.035],
    easeInCirc: [0.6, 0.04, 0.98, 0.335],
    easeInBack: [0.6, -0.28, 0.735, 0.045],
    easeOutQuad: [0.25, 0.46, 0.45, 0.94],
    easeOutCubic: [0.215, 0.61, 0.355, 1.0],
    easeOutQuart: [0.165, 0.84, 0.44, 1.0],
    easeOutQuint: [0.23, 1.0, 0.32, 1.0],
    easeOutSine: [0.39, 0.575, 0.565, 1.0],
    easeOutExpo: [0.19, 1.0, 0.22, 1.0],
    easeOutCirc: [0.075, 0.82, 0.165, 1.0],
    easeOutBack: [0.175, 0.885, 0.32, 1.275],
    easeInOutQuart: [0.77, 0.0, 0.175, 1.0],
    easeInOutQuint: [0.86, 0.0, 0.07, 1.0],
    easeInOutSine: [0.445, 0.05, 0.55, 0.95],
    easeInOutExpo: [1.0, 0.0, 0.0, 1.0],
    easeInOutCirc: [0.785, 0.135, 0.15, 0.86],
    easeInOutBack: [0.68, -0.55, 0.265, 1.55]
  }; // end document
});
"use strict";

!function ($) {
  $.fn.retheme_menu = function (args) {
    return this.each(function () {
      var element = $(this);
      var defaults = {
        animatein: "transition.fadeIn",
        animateout: "transition.fadeOut",
        duration: "300",
        easing: "easeIn",
        callback: ""
      };
      var meta = element.data();
      var options = $.extend(defaults, args, meta);
      var selector = {
        item: ".rt-menu__item",
        submenu: ".rt-menu__submenu",
        has_submenu: ".has-submenu",
        menu_main: ".rt-menu__main",
        arrow: ".rt-menu__arrow",
        trigger: ".js-menu-trigger",
        close: ".js-menu-close"
      };
      /**
       * add arrow if menu find submenu
       */

      element.not(".rt-menu--bar-simple").find(selector.item).each(function () {
        if ($(this).find("ul").length > 0) {
          $(this).children("a").css("padding-right", "30px").after("<span class='rt-menu__arrow'><i class='fa fa-angle-down'></i></span>");
        }
      });
      /**
       * Set Trigger
       * if want trigger from another selector
       * work in menu canvas, menu full screen
       */

      if (options.trigger) {
        var trigger = $(options.trigger);
      } else {
        var trigger = element.find(selector.trigger);
      }
      /*=================================================
      * Menu Bar Style
      /*================================================= */


      var menu_horizontal = function menu_horizontal() {
        /*
         * show submenu if hover
         */
        element.find(selector.item).on("mouseenter", function () {
          $(this).addClass("is-active");
          $(this).find(selector.submenu + ":first").velocity("stop").velocity(options.animatein, {
            duration: options.duration,
            easing: options.easing,
            complete: function complete() {
              $(this).addClass("is-active");
              $(this).children(selector.arrow).addClass("is-active");
            }
          });
        });
        /*
         * Hidden submenu if mouse leave
         */

        element.find(selector.item).on("mouseleave", function () {
          $(this).removeClass("is-active");
          $(this).find(selector.submenu + ":first").velocity("reverse", {
            display: "none",
            complete: function complete() {
              $(this).removeClass("is-active");
              $(this).children(selector.arrow).removeClass("is-active");
            }
          });
        });
        /* End menu bar*/
      };
      /*=================================================
      * Menu Dropdown Style
      /*================================================= */


      var menu_vertical = function menu_vertical() {
        element.find(selector.item).on("click", ">" + selector.arrow, function (event) {
          event.preventDefault();

          if ($(this).hasClass("is-active")) {
            $(this).removeClass("is-active");
            $(this).parent().children(selector.submenu).velocity('stop').velocity("slideUp", {
              duration: options.duration
            });
          } else {
            $(this).addClass("is-active");
            $(this).parent().children(selector.submenu).velocity("slideDown", {
              duration: options.duration
            });
          }
        });
        /* End menu dropdown */
      };
      /*=================================================
      * MENU INIT
      /*================================================= */


      var menuInit = function menuInit() {
        if (element.hasClass("rt-menu--horizontal")) {
          menu_horizontal();
        } else if (element.hasClass("rt-menu--vertical")) {
          menu_vertical();
        }
      };

      menuInit();
      /* Callback
       * run code if menu render finish
       */

      var callback = options.callback;

      if ($.isFunction(callback)) {
        var parameter = "";
        callback.call(this, parameter);
      }
    });
    /*end each*/
  };
  /*end plugin*/

}(window.jQuery);
"use strict";

;

(function ($, window, document, undefined) {
  $.fn.retheme_modal = function (args) {
    return this.each(function () {
      var element = $(this);
      var defaults = {
        animatein: 'transition.fadeIn',
        animateout: 'transition.fadeOut',
        duration: '300',
        easing: 'easeIn'
      };
      var meta = element.data();
      var options = $.extend(defaults, args, meta);
      var selector = {
        close: '.js-modal-close'
      };

      var open = function open() {
        element.velocity('transition.fadeIn');
        /** modal inner show */

        element.find('.rt-modal__inner').velocity(options.animatein, {
          duration: options.duration,
          easing: options.easing
        }).addClass('is-active');
        $("html").css("overflow-y", "hidden");
      };

      var close = function close() {
        element.velocity('reverse', {
          display: 'none'
        });
        /** modal inner hidden */

        element.find('.rt-modal__inner').velocity(options.animateout, {
          duration: options.duration,
          easing: options.easing
        }).removeClass('is-active');
        $('html').css('overflow-y', 'scroll');
      };

      if (options.action == 'open') {
        open();
      }
      /**
       * Modal action on click
       */


      $(options.trigger).on('click', function (event) {
        event.preventDefault();
        open();
      });
      /**
       * hidden if overlay back click
       */

      $(selector.close).on("click", function (event) {
        event.preventDefault();
        close();
      });
    });
  };
})(jQuery, window, document);
"use strict";

!function ($) {
  $.fn.retheme_search = function (args) {
    return this.each(function () {
      var element = $(this);
      var defaults = {
        animatein: 'transition.fadeIn',
        animateout: 'transition.fadeOut',
        duration: '300',
        easing: 'easeIn'
      };
      var selector = {
        close: '.js-search-close',
        trigger: '.js-search-trigger'
      };
      var meta = element.data();
      var options = $.extend(defaults, args, meta);

      var active = function active() {
        if (element.hasClass('rt-search--modal')) {
          element.velocity('transition.shrinkIn', {
            duration: options.duration,
            easing: options.easing,
            display: 'flex'
          }).addClass('is-active');
          element.find('.rt-search__inner').velocity('transition.slideUpBigIn', {
            delay: 300,
            duration: options.duration,
            easing: options.easing,
            display: 'flex'
          });
          $("html").css("overflow-y", "hidden");
        } else {
          element.velocity({
            translateY: ['0%', '-100%']
          }, {
            duration: options.duration,
            easing: options.easing,
            display: 'block',
            complete: function complete(elements) {
              element.addClass('is-active');
            }
          });
        }
      };

      var deactive = function deactive() {
        element.velocity('reverse', {
          display: 'none'
        }).removeClass('is-active');

        if (element.hasClass('rt-search--modal')) {
          element.find('.rt-search__inner').velocity('reverse', {
            display: 'none'
          });
          $("html").css("overflow-y", "scroll");
        }
      };

      $(selector.trigger).on('click', function (event) {
        event.preventDefault();

        if (element.hasClass('is-active')) {
          deactive();
        } else {
          active();
        }
      }); // // disable clik self element
      // element.on('click', function (event) {
      //  event.preventDefault();
      // });
      // close

      $(selector.close).on('click', function (event) {
        event.preventDefault();

        if (element.hasClass('is-active')) {
          deactive();
        }
      });
    });
    /*end each*/
  };
  /*end plugin*/

}(window.jQuery);
"use strict";

(function ($, window, document, undefined) {
  $.fn.retheme_sidepanel = function (args) {
    return this.each(function () {
      var element = $(this);
      var defaults = {
        animatein: "transition.slideBigIn",
        animateout: "transition.slideBigOut",
        duration: 500,
        easing: "easeIn",
        active: false
      };
      var meta = element.data();
      var options = $.extend(defaults, args, meta);
      var selector = {
        trigger: ".js-sidepanel-trigger",
        close: ".js-sidepanel-close",
        panel: ".rt-sidepanel__inner",
        overlay: ".rt-sidepanel__overlay"
      };
      /**
       * Check meta trigger null or not
       */

      if (options.trigger) {
        var trigger = $(options.trigger);
      } else {
        var trigger = $(selector.trigger);
      }
      /**
       * Open panel
       */
      // check panel position


      var position = {
        translateX: ["0%", "100%"]
      };

      if (element.hasClass("rt-sidepanel--left")) {
        var position = {
          translateX: ["0%", "-100%"]
        };
      }

      if (element.hasClass("rt-sidepanel--bottom")) {
        var position = {
          translateY: ["0%", "100%"]
        };
      }

      var open = function open() {
        element.velocity("transition.fadeIn");
        $(selector.panel).velocity("stop").velocity(position, {
          duration: options.duration,
          easing: options.easing,
          display: "block"
        });
        trigger.addClass("is-active");
        $("html").css("overflow-y", "hidden");
      };
      /**
       * Close panel
       */


      var close = function close() {
        element.velocity("reverse", {
          display: "none"
        });
        $(selector.panel).velocity("reverse", {
          display: "none"
        });
        trigger.removeClass("is-active");
        $("html").css("overflow-y", "scroll");
      };
      /**
      * set default panel active
      */


      if (options.action == "open") {
        open();
      } else {
        /**
        * set active with trigger
        */
        trigger.on("click", function (event) {
          event.stopPropagation();
          open();
        });
      }
      /**
       * Close side panel and backdrop
       */


      $(selector.close).click(function (event) {
        event.stopPropagation();
        close();
      });
    });
  };
})(jQuery, window, document);
"use strict";

jQuery(document).ready(function ($) {
  $.fn.retheme_tabs = function (options) {
    return this.each(function () {
      var element = $(this);
      var defaults = {
        animatein: 'transition.fadeIn',
        animateout: 'transition.fadeOut',
        duration: '300',
        easing: 'easeIn'
      };
      var meta = element.data();
      var options = $.extend(defaults, options, meta);
      var selector = {
        nav: '.rt-tab__title',
        item: '.rt-tab__item'
        /*
         * Event
         */

      };
      element.find(selector.nav).on('click', function (event) {
        event.preventDefault();
        var tab_item = $(this).attr("href");
        /*
         * Close all Tabs
         */

        element.find(selector.nav).removeClass('is-active');
        element.find(selector.item).hide().removeClass('is-active');
        /** active tab menu */

        $(this).addClass('is-active');
        /** Open tab content */

        $(this).parents('.rt-tab').find(tab_item).velocity(options.animatein, {
          duration: options.duration,
          easing: options.easing
        }).addClass('is-active');
      });
    });
  };
});
"use strict";

jQuery(document).ready(function ($) {
  $("html").removeClass("no-js").addClass("js");
  $(".js-menu").retheme_menu();
  $(".js-tab").retheme_tabs();
  $(".js-search").retheme_search();
  $(".js-sidepanel").retheme_sidepanel();
  $(".js-modal").retheme_modal();
  $(".js-accordion").retheme_accordion();
  /*=================================================;
  /* MENU - Responsive desain on mobile
  /*================================================= */

  var main_menu = function main_menu() {
    var element = $(".js-main-menu-canvas");
    var trigger = $(".js-mobile-menu-trigger");
    trigger.on("click", function (event) {
      event.preventDefault();

      if ($(this).hasClass("is-active")) {
        $(this).removeClass("is-active");
        element.velocity("slideUp", {
          duration: "300",
          complete: function complete() {
            $(this).removeClass("is-active");
          }
        });
      } else {
        $(this).addClass("is-active");
        element.velocity("stop").velocity("slideDown", {
          duration: "300",
          complete: function complete() {
            $(this).addClass("is-active");
          }
        });
      }
    });
  };

  main_menu();
  /*=================================================
  *  HEADER
  =================================================== */

  var header = function header() {
    var header_desktop = $(".js-header");
    var header_mobile = $(".js-header-mobile");
    /**
     * Check device if resize windows
     */

    $(window).on("resize", function (e) {
      var windows = $(window).width();

      if (windows >= header_desktop.getData("responsive")) {
        header_desktop.show();
        header_mobile.hide();
      } else {
        header_desktop.hide();
        header_mobile.show();
      }
    }).trigger("resize");
    /* sticky  desktop*/

    $(window).scroll(function (event) {
      if ($(this).scrollTop() > 100 && header_desktop.getData("sticky") == true) {
        header_desktop.addClass("is-sticky");
      } else {
        header_desktop.removeClass("is-sticky");
      }
    });
    /**
    * Header mobile menu handle
    */

    $(window).scroll(function (event) {
      if ($(this).scrollTop() > 100 && header_mobile.getData("sticky") == true) {
        header_mobile.addClass("is-sticky");
      } else {
        header_mobile.removeClass("is-sticky");
      }
    });
  };

  header();
  /* =================================================
   *  MASONRY
   * =================================================== */

  var masonry = function masonry(element) {
    if (jQuery().masonry) {
      // init Masonry
      var $grid = $(element).masonry({
        columnWidth: ".flex-item",
        itemSelector: ".flex-item"
      }); // layout Masonry after each image loads

      $grid.imagesLoaded().progress(function () {
        $grid.masonry("layout").masonry({
          horizontalOrder: true
        });
      });
    }
  };

  masonry(".js-masonry");
  /*=================================================
  *  STICKY ASIDE
  =================================================== */

  var sticky = function sticky() {
    if (jQuery.stick_in_parent) {
      var sticky_aside = $(".js-aside-sticky");
      $(window).on("resize", function (e) {
        var windows = $(window).width();

        if (windows >= 992) {
          sticky_aside.stick_in_parent();
        } else {
          sticky_aside.trigger("sticky_kit:detach");
        }
      }).trigger("resize");
    }
  };

  sticky();

  var slider = function slider(element) {
    if (jQuery().owlCarousel) {
      $(element).each(function (index) {
        // get form slider
        var slider = $("#" + $(this).attr("id"));
        var slider_main = $("#" + $(this).attr("id")).find(".rt-slider__main"); // disable class if not infinity slider

        if (slider.hasData("loop")) {
          slider.find(".js-slider-prev").addClass("is-disable");
        }

        slider_main.owlCarousel({
          loop: slider.getData("loop"),
          animateOut: slider.getData("animateout"),
          animateIn: slider.getData("animatein"),
          autoplay: slider.getData("autoplay"),
          lazyLoad: slider.getData("lazyload"),
          center: slider.getData("center"),
          margin: slider.getData("gap"),
          nav: slider.getData("nav"),
          dots: slider.getData("pagination"),
          stagePadding: slider.getData("padding"),
          autoplayHoverPause: slider.getData("autoplayhoverpause"),
          autoplaySpeed: slider.getData("autoplayspeed"),
          slideSpeed: 2000,
          navText: ["<i class='" + slider.getData("nav-icon-left") + "'></i>", "<i class='" + slider.getData("nav-icon-right") + "'></i>"],
          responsive: {
            0: {
              items: slider.getData("items-sm")
            },
            720: {
              items: slider.getData("items-md")
            },
            960: {
              items: slider.getData("items-lg")
            }
          },
          onTranslated: function onTranslated(element) {
            // disable nav header if first item or last item
            if (slider.find(".owl-item").first().hasClass("active")) {
              slider.find(".js-slider-prev").addClass("is-disable");
              slider.find(".js-slider-next").removeClass("is-disable");
            }

            if (slider.find(".owl-item").last().hasClass("active")) {
              slider.find(".js-slider-prev").removeClass("is-disable");
              slider.find(".js-slider-next").addClass("is-disable");
            }
          },
          onInitialized: function onInitialized(element) {
            // hidden nav if only 1 page
            var pages = element.page.size; // Number of pages

            var items = element.item.count; // Number of items

            if (pages == items || pages > items) {
              slider.find(".rt-slider__nav").hide();
              slider.find(".rt-header-block__nav").hide();
            } else {
              slider.find(".rt-slider__nav").show();
            }
          }
        });
        /**
         * CUSTOM NAVIGATION ON HEADER
         */

        slider.find(".js-slider-prev").click(function (event) {
          slider.find(".rt-slider__main").trigger("prev.owl.carousel");
        });
        slider.find(".js-slider-next").click(function (event) {
          slider.find(".rt-slider__main").trigger("next.owl.carousel");
        });
        slider.css("display", "block");
        /** end each */
      });
    }
  };

  slider(".js-slider");
  /*=================================================;
  /* SLIDER SYNC
  /*================================================= */

  var slider_sync = function slider_sync(element) {
    if (jQuery().owlCarousel) {
      $(element).each(function (index) {
        var slider = $("#" + $(this).attr("id"));
        var slider_1 = slider.find(".rt-slider__main");
        var slider_2 = slider.find(".rt-slider__group");
        var slidesPerPage = 4; //globaly define number of elements per page

        var syncedSecondary = true;
        slider_1.owlCarousel({
          items: 1,
          slideSpeed: 2000,
          nav: false,
          autoplay: true,
          dots: slider.getData("pagination"),
          loop: true,
          responsiveRefreshRate: 200
        }).on("changed.owl.carousel", syncPosition);
        slider_2.on("initialized.owl.carousel", function () {
          slider_2.find(".owl-item").eq(0).addClass("is-current");
        }).owlCarousel({
          dots: false,
          nav: slider.getData("nav"),
          smartSpeed: 200,
          slideSpeed: 500,
          slideBy: slidesPerPage,
          //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
          responsiveRefreshRate: 100,
          margin: slider.getData("gap"),
          navText: ["<i class='" + slider.getData("nav-icon-left") + "'></i>", "<i class='" + slider.getData("nav-icon-right") + "'></i>"],
          responsive: {
            0: {
              items: slider.getData("items-sm")
            },
            720: {
              items: slider.getData("items-md")
            },
            960: {
              items: slider.getData("items-lg")
            }
          }
        }).on("changed.owl.carousel", syncPosition2);

        function syncPosition(el) {
          //if you set loop to false, you have to restore this next line
          //var current = el.item.index;
          //if you disable loop you have to comment this block
          var count = el.item.count - 1;
          var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

          if (current < 0) {
            current = count;
          }

          if (current > count) {
            current = 0;
          } //end block


          slider_2.find(".owl-item").removeClass("is-current").eq(current).addClass("is-current");
          var onscreen = slider_2.find(".owl-item.active").length - 1;
          var start = slider_2.find(".owl-item.active").first().index();
          var end = slider_2.find(".owl-item.active").last().index();

          if (current > end) {
            slider_2.data("owl.carousel").to(current, 100, true);
          }

          if (current < start) {
            slider_2.data("owl.carousel").to(current - onscreen, 100, true);
          }
        }

        function syncPosition2(el) {
          if (syncedSecondary) {
            var number = el.item.index;
            slider_1.data("owl.carousel").to(number, 100, true);
          }
        }

        slider_2.on("click", ".owl-item", function (e) {
          e.preventDefault();
          var number = $(this).index();
          slider_1.data("owl.carousel").to(number, 300, true);
        });
      });
    }
  };

  slider_sync(".js-slider-sync");
  /*=================================================
  *  Go To Top
  =================================================== */

  var gotop = function gotop() {
    var gotop = $(".js-gotop");
    $(window).scroll(function (event) {
      if ($(this).scrollTop() > 80) {
        gotop.addClass("is-active");
      } else {
        gotop.removeClass("is-active");
      }
    });
    gotop.click(function (event) {
      $("body").velocity("scroll", {
        duration: 700
      });
    });
  };

  gotop();
  /*=================================================;
  /* SET ACTIVE
  /*================================================= */

  var toggle_active = function toggle_active(element) {
    $(".js-active-toggle").each(function (index) {
      var action = $(this).getData("action");
      var animationIn = $(this).getData("animationIn");
      var animationDuration = $(this).getData("animationDuration");
      var element = $(this).attr("id");
      $(trigger).on("click", function () {
        if (element.hasClass("is-active")) {
          element.removeClass("is-active");
        } else {
          element.velocity("stop").velocity(animationIn, {
            duration: animationDuration
          }).addClass("is-active");
        }
      });
    });
  };

  toggle_active();
  /*=================================================
  *  Comments
  =================================================== */
  // Show all form comments for single post

  var comments = function comments(elements) {
    var element = $(elements);
    element.find("textarea").on("click, focus", function (event) {
      event.preventDefault();

      if (!element.hasClass("is-active")) {
        $(element).find(".comment-input").velocity("stop").velocity("slideDown", {
          duration: "600",
          display: "block"
        });
        element.addClass("is-active");
      }
    });
  };

  comments(".js-comment");
  /*=================================================
  *  ELEMENTOR REGISTER
  =================================================== */

  var elementor_frontend_hook = function elementor_frontend_hook() {
    elementorFrontend.hooks.addAction("frontend/element_ready/retheme-product.default", function () {
      slider(".js-elementor-slider");
      masonry(".js-elementor-masonry");
    });
    elementorFrontend.hooks.addAction("frontend/element_ready/retheme-post.default", function () {
      slider(".js-elementor-slider");
      masonry(".js-elementor-masonry");
    });
    elementorFrontend.hooks.addAction("frontend/element_ready/retheme-carousel.default", function () {
      slider(".js-elementor-slider");
      masonry(".js-elementor-masonry");
    });
  };

  if ($("body").hasClass("elementor-editor-active")) {
    elementor_frontend_hook();
  } else {
    $(window).on("elementor/frontend/init", function (options) {
      elementor_frontend_hook();
    });
  } // end document

});